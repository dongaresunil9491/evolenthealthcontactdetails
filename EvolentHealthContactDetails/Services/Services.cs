﻿using EvolentHealthContactDetails.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EvolentHealthContactDetails.Services
{
    public static class Services
    {
        public static List<Data.EvolentHealthContactDetail> GetContactDetailsData(string status = null)
        {
            EvolentHealthEntities dataRepository = new EvolentHealthEntities();
            List<Data.EvolentHealthContactDetail> data = null;
            if (string.IsNullOrWhiteSpace(status))
                data = dataRepository.EvolentHealthContactDetails.ToList();
            else if (status.Equals("Inactive", StringComparison.InvariantCultureIgnoreCase) || status.Equals("false", StringComparison.InvariantCultureIgnoreCase))
                data = dataRepository.EvolentHealthContactDetails.Where(w => w.Status.Equals(false)).ToList();
            else if (status.Equals("Active", StringComparison.InvariantCultureIgnoreCase) || status.Equals("True", StringComparison.InvariantCultureIgnoreCase))
                data = dataRepository.EvolentHealthContactDetails.Where(w => w.Status.Equals(true)).ToList();
            else
                data = dataRepository.EvolentHealthContactDetails.ToList();
            return data;
        }
        public static List<Models.ContactDetails> MapData(List<Data.EvolentHealthContactDetail> source, out List<Models.ContactDetails> destination)
        {
            destination = new List<Models.ContactDetails>();
            foreach (Data.EvolentHealthContactDetail s in source)
            {
                destination.Add(new Models.ContactDetails()
                {
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    Email = s.Email,
                    PhoneNumber = s.PhoneNumber,
                    Status = s.Status ? "Active" : "Inactive",
                    Create_Date = s.CREATE_DATE
                });

            }
            return destination;
        }

        public static bool AddContactDetailsData(Models.ContactDetailsInsert resource)
        {
            EvolentHealthEntities dataRepository = new EvolentHealthEntities();
            try
            {
                dataRepository.EvolentHealthContactDetails.Add(new Data.EvolentHealthContactDetail { FirstName = resource.FirstName, LastName = resource.LastName, Email = resource.Email, PhoneNumber = resource.PhoneNumber, Status = resource.Status, CREATE_DATE = resource.Create_Date });
                dataRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                dataRepository.Dispose();
            }
        }

        public static List<Data.EvolentHealthContactDetail> UpdateContactDetailsData(int id, Models.ContactDetailsInsert resource)
        {
            EvolentHealthEntities dataRepository = new EvolentHealthEntities();
            try
            {
                if (!id.Equals(0))
                {
                    var data = dataRepository.EvolentHealthContactDetails.Where(w => w.ID.Equals(id)).ToList();
                    if (data.Count > 1)
                    {
                        throw new Exception("multiple records found");
                    }
                    else if (data.Count <= 0)
                    {
                        throw new Exception("no records found");
                    }
                    else
                    {
                        var model = new Data.EvolentHealthContactDetail { ID = data.FirstOrDefault().ID, FirstName = resource.FirstName, LastName = resource.LastName, Email = resource.Email, PhoneNumber = resource.PhoneNumber, Status = resource.Status, CREATE_DATE = resource.Create_Date };
                        dataRepository.Entry(data.FirstOrDefault()).CurrentValues.SetValues(model);
                        dataRepository.SaveChanges();
                        return dataRepository.EvolentHealthContactDetails.Where(w => w.FirstName.Equals(resource.FirstName, System.StringComparison.InvariantCultureIgnoreCase)).ToList();
                    }
                }
                else
                {
                    throw new Exception("no records found");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dataRepository.Dispose();
            }
        }

        public static bool DeleteContactDetailsData(bool status)
        {
            EvolentHealthEntities dataRepository = new EvolentHealthEntities();
            try
            {
                List<Data.EvolentHealthContactDetail> data = null;
                data = dataRepository.EvolentHealthContactDetails.Where(w => w.Status.Equals(status)).ToList();
                if (data.Count >= 1)
                {
                    data.ForEach(d => dataRepository.EvolentHealthContactDetails.Remove(d));
                    dataRepository.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                dataRepository.Dispose();
            }
        }
    }
}