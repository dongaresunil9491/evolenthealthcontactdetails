﻿using EvolentHealthContactDetails.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace EvolentHealthContactDetails.Controllers
{
    [RoutePrefix("api/contactDetails")]
    public class EvolentHealthContactDetailsController : ApiController
    {
        /// <summary>
        /// If input status string will Inactve of false it will show list of Inactive contact detalis.
        /// If input status string will actve of false it will show list of active contact detalis.
        /// If input status string will Empty or not(Inactve,false,active,true) it will show all list of contact detalis.
        /// If will display all contacts if status will Empty or not(Inactve)
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("", Name = RouteNames.ContactsRoute.GetContactDetails)]
        public IHttpActionResult GetContactDetails( [FromUri] string status =null)
        {
            List<Data.EvolentHealthContactDetail> resources = null;
            if (string.IsNullOrWhiteSpace(status))
            {
                resources = Services.Services.GetContactDetailsData(status);
            }
            else
            {
                resources = Services.Services.GetContactDetailsData(status);
                if (resources.Count < 1)
                {
                    return NotFound();
                }
            }
            Services.Services.MapData(resources, out List<ContactDetails> result);
            return Ok(result);
        }
        /// <summary>
        /// It will use to add new contact detail
        /// </summary>
        /// <param name="contactDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("", Name = RouteNames.ContactsRoute.AddContactDetails)]
        public IHttpActionResult AddContactDetails(ContactDetailsInsert contactDetails)
        {
            bool response = false;
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(contactDetails.FirstName) && string.IsNullOrWhiteSpace(contactDetails.LastName) &&
                    string.IsNullOrWhiteSpace(contactDetails.Email) && string.IsNullOrWhiteSpace(contactDetails.PhoneNumber))
                {
                    BadRequest(ModelState);
                }
                else
                {
                    if (contactDetails.Create_Date == DateTime.MinValue)
                        contactDetails.Create_Date = DateTime.UtcNow;
                    if (Services.Services.AddContactDetailsData(contactDetails))
                    {
                        response = true;
                    }
                }
            }
            else
            {
                BadRequest(ModelState);
            }

            return Ok(response);
        }
        /// <summary>
        /// Need to give uniq ID number to update the contact details
        /// </summary>
        /// <param name="id"></param>
        /// <param name="contactDetails"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("", Name = RouteNames.ContactsRoute.UpdateContactDetails)]
        public IHttpActionResult UpdateContactDetails(int id, [FromBody]ContactDetailsInsert contactDetails)
        {
            List<Data.EvolentHealthContactDetail> resources = null;
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(contactDetails.FirstName) && string.IsNullOrWhiteSpace(contactDetails.LastName) && string.IsNullOrWhiteSpace(contactDetails.Email) && string.IsNullOrWhiteSpace(contactDetails.PhoneNumber))
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    if (contactDetails.Create_Date == DateTime.MinValue)
                        contactDetails.Create_Date = DateTime.UtcNow;
                    resources = Services.Services.UpdateContactDetailsData(id, contactDetails);
                }
            }
            else
            {
                return BadRequest();
            }
            Services.Services.MapData(resources, out List<ContactDetails> result);
            return Ok(result);
        }
        /// <summary>
        /// re
        /// </summary>
        /// <param name="status"></param>
        /// <returns>
        /// If select false it will delete all Inactive contacts
        /// if select true it will delete all acitive contacts
        /// </returns>
        [HttpDelete]
        [Route("", Name = RouteNames.ContactsRoute.DeleteContactDetails)]
        public IHttpActionResult DeleteContactDetails([FromUri] bool status)
        {
            if (Services.Services.DeleteContactDetailsData(status))
            {
                return Ok(true);
            }
            else
                BadRequest();
            return Ok(false);
        }
        /// <summary>
        /// It will delete only Inactive ontacts
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteInactiveContacts", Name = RouteNames.ContactsRoute.DeleteInactiveContacts)]
        public IHttpActionResult DeleteInactiveContactsDetails()
        {
            if (Services.Services.DeleteContactDetailsData(false))
            {
                return Ok(true);
            }
            else
                BadRequest();
            return Ok(false);
        }
    }
    public partial class RouteNames
    {
        public partial class ContactsRoute
        {
            public const string GetContactDetails = "GetContactDetails";
            public const string AddContactDetails = "AddContactDetails";
            public const string UpdateContactDetails = "UpdateContactDetails";
            public const string DeleteContactDetails = "DeleteContactDetails";
            public const string DeleteInactiveContacts = "DeleteInactiveContacts";

        }
    }
}
